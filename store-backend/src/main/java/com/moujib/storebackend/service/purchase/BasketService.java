package com.moujib.storebackend.service.purchase;

import com.moujib.storebackend.model.purchase.Basket;

public interface BasketService {

    Basket getBasketById(int id);
}
