package com.moujib.storebackend.service.product;

import com.moujib.storebackend.model.product.Product;

public interface ProductService {

    Product getProductById(int id);
}
