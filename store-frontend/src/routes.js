import Home from './components/Home';
import AdminController from './components/admin/AdminController';
import ProductsController from './components/shop/ProductsController'

export const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/products', name : 'ProductsController', component: ProductsController},
  { path: '/adminPage', name: 'AdminController', component: AdminController }
];
