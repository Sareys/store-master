// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'
import {routes} from './routes'
import axios from 'axios'

axios.defaults.baseURL = 'http://localhost:8080'
axios.defaults.headers = {
  'Access-Control-Allow-Origin': 'http://localhost:8081'
}

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
